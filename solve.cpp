#include <iostream>
#include <vector>
#include <string>
void remove(std::string &a, int i) {
	if(a.empty()) return;
	a=a.substr(0,i)+a.substr(i+1);
}
int common(std::string a, std::string b) {
	int c = 0;
	for(int i = 0; i<= a.length()-1;i++) {
		if(a[i]==b[i]) c++;
	}
	return c;
}
int sumcommon(std::vector<std::string> a,std::string b) {
	int sum = 0;
	for(std::string i:a) {
		sum+=common(i,b);
	}
	return sum;
}
int main() {
	std::vector<std::string> passwords = {};
	while(true) {
		std::string temp;
		std::cin >> temp;
		if(temp.compare("STOP")!=0) {
			passwords.push_back(temp);
		} else break;
	}
	while(true) {
		std::string best;int bestc=0;
		for(std::string i:passwords) {
			if(sumcommon(passwords,i)>bestc) {
				bestc=sumcommon(passwords,i);
				best=i;
			}
		}
		std::cout << best << std::endl;
		int num;
		std::cin >> num;
		for(int i = 0;i <= passwords.size()-1;i++) {
			std::cout << best << "," << passwords[i] << common(best,passwords[i]) <<std::endl;
			if(common(best,passwords[i])!=num) {
				passwords.erase(passwords.begin()+i);
				i--;
			}
		}
	}
	return 0;
}
